import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./Sidebar.css"

import { getMembershipContent } from '../../store/actions/membershipContentAction'
import { Link } from "react-router-dom";

function Sidebar() {
    const dispatch = useDispatch()

    const {membershipContent, isLoading} = useSelector(state => state.membershipContent)
    const [showExam, setShowExam] = useState(false)
    
    const [toggleMenu, setToggleMenu] = useState(false)
    const [screenWidth, setScreenWidth] = useState(window.innerWidth)

    useEffect(() => {
        if (!membershipContent || membershipContent === "") {
            dispatch(getMembershipContent())        
        }

        if (membershipContent) {
            let count = 0 
            for (let i = 0 ; i < membershipContent.length ; i++) {
                if (membershipContent.done === true) {
                    count++
                }
            }

            if (membershipContent.length === count) {
                setShowExam(true)
            }
        }
    },[])


    const toggleNav = () => {
        setToggleMenu(!toggleMenu)
    }
    
    useEffect(() => {
    
        const changeWidth = () => {
            setScreenWidth(window.innerWidth);
        }
    
        window.addEventListener('resize', changeWidth)
    
        return () => {
            window.removeEventListener('resize', changeWidth)
        }
    
    }, [])

    if (isLoading) return (
        <>
            <div className="side-load">Still Loading...</div>
        </>
    )

    return (
        <>
        <button onClick={toggleNav} className="btn-sidenav"></button>
        {(toggleMenu || screenWidth > 425) && (
        <div className="sidenav">
            <div className="scroll-modul">
            <Link to="/main/module">Modul Pembelajaran</Link>
            {
                !!membershipContent ? membershipContent.map((content) => <Link to={"/show/module/" + content.id} className={content.done ? "done-content" : "content"}  key={content.id}>{content.title}: {content.description}</Link>) : <Link>Error</Link>
            }
            {/* {showExam ? <Link to={path.memberExam}>Exams</Link> : <p className="disable">Exams</p> } */}
            <Link to="/member/exam">Exams</Link>
            </div>
        </div>  )}
        </>
    )
}

export default Sidebar