import React, {useState, useEffect} from "react"
import { useDispatch} from "react-redux"
import { Link, useHistory } from "react-router-dom"

import { membershipLogout } from "../../store/actions/dataMembershipAction"

import logoNav from '../../asset/img/logo-mis-color.png'

import "./Navbar.css"
import path from "../../routes/route"

function Navbar() {
    const history = useHistory()
    const dispatch = useDispatch()

    const [toggleMenu, setToggleMenu] = useState(false)
    const [screenWidth, setScreenWidth] = useState(window.innerWidth)

    const toggleNav = () => {
        setToggleMenu(!toggleMenu)
    }
    
    useEffect(() => {
    
        const changeWidth = () => {
            setScreenWidth(window.innerWidth);
        }
    
        window.addEventListener('resize', changeWidth)
    
        return () => {
            window.removeEventListener('resize', changeWidth)
        }
    
    }, [])
    


    const logoutMembership = () => {
        return dispatch(membershipLogout())
    }

    function imageClick(){
        history.push(path.mainMenu)
    }

    return (
        <div className="topnav">
            <button onClick={toggleNav} className="btn-nav"></button>
            <img className="image-nav" src={logoNav} alt="nav-logo" onClick={imageClick}/>
            {(toggleMenu || screenWidth > 425) && (
            <div className="navigation">
                <Link to={path.mainMenu}>Beranda</Link>
                <Link to={path.mainModule}>Modul Pembelajaran</Link>
                <Link to={path.talkshow}>Talkshow</Link>
                <div className="dropdown">
                    <button className="dropbtn">Nama User <i className="fa fa-caret-down"></i></button>
                    <div className="dropdown-content">
                        <Link to={path.memberProfile}>Profile</Link>
                        <Link to={path.certificate}>Sertifikat</Link>
                        <Link to={path.FAQ}>FAQ</Link>
                        <Link to={path.login} onClick={logoutMembership}>Logout</Link>
                    </div>
                </div>
            </div>   )}
            
        </div>
    )
}

export default Navbar