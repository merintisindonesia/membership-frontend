import { Switch, Route } from "react-router"

import PrivateRoute from "./routes/PrivateRoute.js"
import OnlyPublicRoute from "./routes/OnlyPublicRoute.js"
import path from "./routes/route"

import {
  Home,
  Login,
  MainMenu,
  ModulPembelajaran,
  TalkShow,
  ProfileMembership,
  FrequentlyAskQuestion,
  Certificate,
  MembershipExam,
  StartExam,
  MainModulPembelajaran,
  ResultScore,
  RegisterMembership,
  LandingPage,
} from "./pages/index.js"

function App() {
  return (
    <div className="App">
        <Switch>
          <OnlyPublicRoute exact path={path.login} component={Login}/>
          <PrivateRoute exact path={path.mainMenu} component={MainMenu}/>
          <PrivateRoute exact path={path.module} component={ModulPembelajaran}/>
          <PrivateRoute exact path={path.mainModule} component={MainModulPembelajaran}/>
          <PrivateRoute exact path={path.talkshow} component={TalkShow}/>
          <PrivateRoute exact path={path.memberProfile} component={ProfileMembership}/>
          <Route exact path={path.FAQ} component={FrequentlyAskQuestion}/>
          <PrivateRoute exact path={path.certificate} component={Certificate}/>
          <PrivateRoute exact path={path.memberExam} component={MembershipExam}/>
          <PrivateRoute exact path={path.startExam} component={StartExam}/>
          <PrivateRoute exact path={path.resultScore} component={ResultScore}/>
          <Route exact path={path.root} component={Home}/>
          <OnlyPublicRoute exact path={path.memberRegister} component={RegisterMembership}/>
          <OnlyPublicRoute exact path={path.landingPage} component={LandingPage}/>
          <Route path={path.any}/>
        </Switch>
    </div>
  );
}

export default App;
