import React from 'react'
import { useHistory } from 'react-router-dom'

// import "./LandingPage.css"
// import "./theme.css"

const LandingPage = () => {
    const history = useHistory()

    const toRegister = () => {
        history.push({
            pathname:"/member/registration",
            // nanti ini diganti sesuai kebutuhan user untuk type dan status
            state: {
                type: "Perintis",
                status: "basic",
                userId : "68"
            }
        })
    }

    return ( <>
        <main class="main" id="top">
            <nav class="navbar navbar-light sticky-top" data-navbar-darken-on-scroll="900">
                <div class="container pt-2"><a class="navbar-brand" href="https://merintisindonesia.com"> <img src="https://merintisindonesia.com/assets/img/Logo-MI.png" alt="Merintis Indonesia" className="img-navbar"/></a>
                </div>
            </nav>
            <section class="mt-6">

            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-9 col-lg-4 col-xl">
                    <h1 class="display-3 lh-sm">Merintis Indonesia Membership</h1>
                    <p class="fs-2">Cara baru belajar bisnis</p>
                    <button class="btn btn-primary mt-3" onClick={toRegister}>Daftar Disini</button>
                    </div>
                </div>
            </div>
            </section>
        </main>
    </>)
}

export default LandingPage