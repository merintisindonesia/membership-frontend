import React, { useEffect } from "react";
import Navbar from '../../components/navbar/Navbar'
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux";

import { getMembershipProfile } from "../../store/actions/dataMembershipAction.js"

import path from "../../routes/route.js";

import "./MainMenu.css"

function MainMenu() {
    const history = useHistory()
    const dipatch = useDispatch()

    const {membershipProfile, isLoading} = useSelector(state => state.dataMembership)

    useEffect(() => {
        if (!membershipProfile) {
            dipatch(getMembershipProfile())
        }
    },[])

    function toModule() {
        history.push(path.mainModule)
        return
    }

    function toTalkShow() {
        history.push(path.talkshow)
        return
    }

    if (isLoading) {
        return <h1 className="loading"><div class="loader-main-menu margin-right-3"></div>Please wait, loading...</h1>
    } else {
        return (
            <div className="main-menu">
                <Navbar/>
                {
                    !!membershipProfile ? (
                    <>
                        <h1 className="h1-main-menu">Halo, {membershipProfile.nm_lengkap}</h1>
                        <h2 className="h2-main-menu">{membershipProfile.type} {membershipProfile.status}</h2>
                    </>
                    ) : <h1>masih mendapatkan data</h1>
                }
                <div className="container">
                    <div className="border-container">
                        <div className="border-box">
                            <h2 className="tema">MODUL PEMBELAJARAN</h2>
                            <p>Modul pembelajaran yang memberikan pembekalan berupa<span>skill set</span> dan <span>knowledge</span> fundamental dalam mewujudkan ide bisnis</p>
                        </div>
                        <button className="button-yellow" onClick={toModule}>MULAI</button>
                    </div>
                    <div className="border-container">
                        <div className="border-box">
                            <h2 className="tema">ONLINE TALKSHOW</h2>
                            <p>Dapat diakses 12x dalam setahun melalui media Live Zoom di mana peserta bisa berdiskusi dengan mentor selama 1 jam dengan topik tertentu dalam bisnis</p>
    
                        </div>
                        <button className="button-yellow" onClick={toTalkShow}>MULAI</button>
                    </div>
    
                    <div className="border-container">
                        <div className="border-box coming">
                            <h2 className="tema">COMMUNITY SHARING</h2>
                            <p>Forum yang digunakan untuk networking dan pitching bersama sesama pengusaha dan para stakeholder bisnis yang diadakan 1x dalam sebulan</p>
    
                        </div>
                        <button className="button-coming">SEGERA HADIR</button>
                    </div>
    
                    <div className="border-container">
                        <div className="border-box coming">
                            <h2 className="tema">BUSINESS EXPO</h2>
                            <p>Pemeran Ide bisnis sekaligus tahapan launching yang diadakan selama 1x dalam 1 tahun secara online maupun offline</p>
                        </div>
                        <button className="button-coming">SEGERA HADIR</button>
                    </div>
                </div>
            </div>
        )
    }
}


export default MainMenu;