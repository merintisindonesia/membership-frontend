import React, { useEffect } from 'react';

import Navbar from '../../components/navbar/Navbar'

import "./Talkshow.css"
import { useDispatch, useSelector } from 'react-redux';
import { dataTalkshowVideos } from '../../store/actions/talkshowAction';


function TalkShow() {
    const dispatch = useDispatch()

    const { isLoading, talkshows} = useSelector(state => state.talkshow)

    useEffect(() => {
        if(!talkshows) {
            dispatch(dataTalkshowVideos())
        }
    },[talkshows, dispatch])
    
    console.log(talkshows)

    if (isLoading) {
        return (
            <>
                <Navbar/>
            </>
        )
    }
    return(
        <div className="main-topic">
            <Navbar/>
            <div className="topic-content">
            <h1>Online Talkshow</h1>
        
            <ul className="card-wrapper">
                {
                    talkshows ? talkshows.map(talkshow => {
                        return (<li className="card" key={talkshow.id}><a href={talkshow.video_url} target="_blank" rel="noreferrer">
                            <img srcSet={talkshow.image_url} alt="sub-talkshow-video"/>
                            <h3>{talkshow.title}</h3></a>
                        </li>)
                    }) : <li></li>
                }
            </ul>
            </div>
        </div>
    )
}


export default TalkShow