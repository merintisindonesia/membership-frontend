import React, { useState } from "react"

import "./FrequentlyAskQuestion.css"
import Navbar from "../../components/navbar/Navbar"

function FrequentlyAskQuestion() {
    const [state, setState] = useState({
        sectionOne : false,
        sectionTwo : false,
        sectionThree : false,
        sectionFour : false,
        sectionFive : false,
        sectionSix : false,
        sectionSeven : false,
        sectionEight : false,
        sectionNine : false,
    })

    function changeClick(e) { 
        setState(!state[e.target.name] ? {[e.target.name] : true} : {[e.target.name] : false})
    }

    return (
        <>
        { !!localStorage.getItem("access_token") ? <Navbar/> : <div className="no-login"></div>}
        <div className={!!localStorage.getItem("access_token") ? "faq-main-login" : "faq-main"}>
            <h1>Frequently Ask Question (FAQ)</h1>
            <p>
            Lihat daftar di bawah untuk pertanyaan-pertanyaan yang sering diajukan seputar Membership Merintis Indonesia. 
            Jika Sedulur memiliki pertanyaan atau keluhan yang tidak ada di dalam daftar, 
            mohon hubungi kami di email <span>cs@merintisindonesia.com</span> atau WhatsApp kami di nomor +6285785036770.
            </p>

            <button name="sectionOne" className={ state.sectionOne ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Bagaimana cara membeli paket Membership ? <i className="fa-dropdown"></i></button>
            <div className={state.sectionOne ? "panel-block" : "panel"}>
                <p>Pembelian paket Membership dapat dilakukan dengan login ke website Merintis Indonesia menggunakan akun yang terdaftar, masuk ke halaman <a href="#faq-main">Membership</a>, dan klik tombol “Daftar Sekarang”. Sistem akan mengarahkan ke formulir checkout, lakukan pembayaran, dan petunjuk akses ke website Membership akan dikirimkan ke email.</p>
            </div>

            <button name="sectionTwo" className={ state.sectionTwo ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Apa saja yang akan saya dapatkan jika saya mendaftar di Membership ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionTwo ? "panel-block" : "panel"}>
                <p>Sedulur akan mendapatkan akses ke Modul Pembelajaran (jumlah modul sesuai dengan pilihan paket yang dipilih), akses ke Talkshow (jumlah talkshow sesuai dengan pilihan paket yang dipilih), Community Sharing (segera hadir), dan Business Expo (segera hadir).</p>
            </div>

            <button name="sectionThree" className={ state.sectionThree ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Bagaimana cara pembayaran di Membership Merintis Indonesia ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionThree ? "panel-block" : "panel"}>
                <p>Saat ini kami memiliki tiga cara pembayaran: transfer ke Virtual Account bank (BCA, BNI, BRI, Mandiri), transfer ke Gopay, atau scan QRIS melalui e-wallet (OVO, LinkAja).</p>
            </div>

            {/* 4 */}
            <button name="sectionFour" className={ state.sectionFour ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Saya sudah melakukan pembayaran namun belum mendapatkan email akses ke website Membership, apa yang harus saya lakukan ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionFour ? "panel-block" : "panel"}>
                <p>Mohon hubungi <span>cs@merintisindonesia.com</span> atau melalui Whatsapp ke nomor berikut: +6285785036770.</p>
            </div>

            <button name="sectionFive" className={ state.sectionFive ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Saya tidak dapat masuk ke website Membership, apa yang harus saya lakukan ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionFive ? "panel-block" : "panel"}>
                <p>Mohon periksa kembali username dan password yang digunakan untuk login ke website Membership. Gunakan akses yang sama untuk login ke website Merintis Indonesia.</p>
            </div>

            <button name="sectionSix" className={ state.sectionSix ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Saya sudah memiliki tipe Membership Perintis, bagaimana cara agar saya dapat membeli tipe Membership Pengembang (dan sebaliknya) ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionSix ? "panel-block" : "panel"}>
                <p>Saat ini kami menerapkan sistem 1 pengguna untuk 1 tipe Membership, namun kami sedang mengembangkan cara agar 1 pengguna dapat memiliki 2 tipe Membership sekaligus. Sementara itu, Sedulur dapat membeli tipe Membership lainnya menggunakan akun Merintis Indonesia yang berbeda.</p>
            </div>

            {/* {7} */}
            <button name="sectionSeven" className={ state.sectionSeven ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Bagaimana cara melakukan upgrade paket Membership dari Basic ke Premium ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionSeven ? "panel-block" : "panel"}>
                <p>Lakukan pembelian paket Membership dengan cara yang sama dengan pembelian paket sebelumnya, namun pilihlah pilihan paket “Premium” saat berada di formulir checkout.</p>
            </div>

            <button name="sectionEight" className={ state.sectionEight ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Bagaimana cara mengetahui informasi promo Membership Merintis Indonesia ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionEight ? "panel-block" : "panel"}>
                <p>Cek promo yang tersedia di halaman Promo, atau cek akun sosial media Merintis Indonesia.</p>
            </div>

            <button name="sectionNine" className={ state.sectionNine ? "active accordion" : "accordion"} onClick={e => changeClick(e)}>Saya sudah lulus tes kompetensi, bagaimana cara saya mendapatkan sertifikat ?  <i className="fa-dropdown"></i></button>
            <div className={state.sectionNine ? "panel-block" : "panel"}>
                <p>Sedulur dapat menunggu sertifikat tampil di halaman Sertifikat maksimal 3x24 jam setelah dinyatakan lulus tes kompetensi. Kami juga akan mengirimkan email softcopy sertifikat ke email yang terdaftar di Merintis Indonesia. Jika sertifikat belum diterima sampai batas waktu maksimal, mohon hubungi cs@merintisindonesia.com atau melalui Whatsapp ke nomor berikut: +6285785036770.</p>
            </div>
        </div>
        </>
    )
}

export default FrequentlyAskQuestion