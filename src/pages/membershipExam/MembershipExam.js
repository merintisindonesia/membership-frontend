import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"

import { getMembershipExam, createNewMembershipExam } from "../../store/actions/membershipExamAction"

import "./MembershipExam.css"
import { Link, useHistory } from "react-router-dom"

function Exam() {
    const dispatch = useDispatch()
    const history = useHistory()

    const { membershipExams, newMembershipExam, isLoading, loadingNewMemberExam } = useSelector(state => state.membershipExam)

    const [getCertificate, setGetCertificate] = useState(false)
    const [resumeExamCondition, setResumeExamCondition] = useState(false)


    useEffect(() => {
        if(!membershipExams) {
            dispatch(getMembershipExam())
        }

        if(membershipExams) {
            membershipExams.map(memberExam => {
                if(memberExam.score >= 75) {
                    setGetCertificate(true)
                }

                if (new Date(memberExam.time_length) >= new Date() && memberExam.count_get_exam === 1) {
                    setResumeExamCondition(true)
                }

                return memberExam
            })
        }

        if(newMembershipExam) {
            history.push({
                pathname: "/start/exam/" + newMembershipExam.id
            })
        }
    },[membershipExams, newMembershipExam])

    function goStartTest() {
        dispatch(createNewMembershipExam())
    }

    function goToCertificate() {
        history.push({
            pathname: "/member/certificate"
        })
    }

    if (isLoading) {
        return (
            <>
                <Navbar/>
                <h1 className="loading"><div class="loader-main-menu margin-right-3"></div> Loading Get Data </h1>
            </>
        )
    }

    return (
    <>
        <Navbar/>
        <Sidebar/>
        <div className="membership-exam">
            <h1>Ikuti Tes Kompetensi</h1>
            <p>Tes Kompetensi berikut akan mengukur kemampuan kamu dalam memahami topik-topik yang telah dipelajari sebelumnya. Tes ini terdiri dari 15 pertanyaan dan 3 pilihan jawaban. Kamu harus melewati skor 75 untuk dapat lulus tes dan mendapat sertifikat. Jika kamu gagal dalam tes, kamu dapat mengulang tes maksimal 3 kali dalam 1 minggu. Selamat mengerjakan!</p>
            
            <h2>Tes yang sudah diikuti</h2>
            {
                !!membershipExams ? (
                    membershipExams.map(memberExam => {
                        if(new Date() >= new Date(memberExam.time_length) || memberExam.score > 0 || memberExam.count_get_exam === 2) {
                            return (
                                <h3 key={memberExam.id}>waktu tes :{new Date(memberExam.created_at).toLocaleString("en-GB")}, score : {memberExam.score}</h3>
                            )
                        } else {
                            return (
                                <>
                                <h3 key={memberExam.id}>waktu tes :{new Date(memberExam.created_at).toLocaleString("en-GB")}</h3>
                                <Link to={"/start/exam/" + memberExam.id} >Lanjutkan</Link>
                                </>
                            )
                        }
                    })
                ) : <p></p>
            }

        </div>

        <div className="position-right">
            { getCertificate ? <button className="button-exam" onClick={goToCertificate}>Lihat Sertifikat</button> : <p></p>}
            { loadingNewMemberExam ? <button className="button-gray mobile">Menyiapkan tes</button> : resumeExamCondition ? <p></p> : <button className="button-exam mobile" onClick={goStartTest}> MULAI TES</button> } 
        </div>
    </>)

}

export default Exam