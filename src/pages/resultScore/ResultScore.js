import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import { useHistory } from "react-router-dom"
import "./ResultScore.css"
import { useSelector } from "react-redux"

const ResultScore = (props) => {
    const history = useHistory()

    const { correctResult, totalQuestion, pass, finalScore } = (props.location && props.location.state) || {}

    console.log(correctResult, totalQuestion, pass, finalScore)

    const { isLoading } = useSelector(state => state.membershipExam)  

    const goBackMainExam = () => {
        history.push("/member/exam")
    }

    console.log(isLoading)

    if (isLoading) {
        return (
            <>
                <Navbar/>
                <Sidebar/>
                <div className="result-score">Please wait, still counting</div>
            </>
        )
    }

    return(
        <>
        <Navbar/>
        <Sidebar/>
        <div className="result-score">
            <h1>Selamat, Sedulur selesai mengerjakan tes kompetensi!!</h1>
            <h2>anda mendapat hasil : {correctResult} / {totalQuestion}</h2>
            <h2>Score yang didapat  : {finalScore}</h2>
            {pass ? <p>Dengan mengikuti Tes Kompetensi sebelumnya, kamu diharapkan dapat memanfaatkan ilmu dan mengaplikasikannya pada bisnis kamu. Lihat Sertifikatmu pada <a href="#">link berikut.</a></p> : <p>Jangan sedih, ya! Kamu masih bisa mengulang tes lagi sesuai dengan ketentuan maksimal 3 kali tes dalam 1 minggu. Semangat!</p>}
            <button className="btn-back"onClick={goBackMainExam}>Kembali</button>
        </div>
        </>
    )
}

export default ResultScore