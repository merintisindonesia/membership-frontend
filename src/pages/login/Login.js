import React, { useEffect, useState } from 'react';
import "./Login.css"
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import logoMI from "../../asset/img/Logo-MI.png"

import { clearError, membershipLogin } from "../../store/actions/dataMembershipAction"

import path from "../../routes/route.js"

function LoginMembership() {
    const dispatch = useDispatch()
    const history = useHistory()

    const [email, setEmail] = useState("")
    const [password, setpassword] = useState("")

    const [ errorLogin, setErrorLogin] = useState("")

    const { errorData, isLoading, membershipLoginData} = useSelector(state => state.dataMembership)

    useEffect(() => {
        if (localStorage.getItem("access_token")) {
            console.log("login_success")
            return history.push(path.mainMenu)
        }
    },[history])

    const inputEmail = (e) => {
        e.preventDefault()
        setEmail(e.target.value)
        setErrorLogin("")
    }

    const inputPass = (e) => {
        e.preventDefault()
        setpassword(e.target.value)
        setErrorLogin("")
    }

    const submitLogin = (e) => {
        e.preventDefault()
        if (email === "" || password === "") {
            setErrorLogin("mohon lengkapi email & password dengan benar")
            dispatch(clearError())
        } else if (!function(em){
            var re = /\S+@\S+\.\S+/;
            return re.test(em);
        }(email)) {
            setErrorLogin("mohon masukkan format email yang benar")
            dispatch(clearError())
        }  else {
            const payload = {
                email : email,
                password : password
            }
            dispatch(membershipLogin(payload))
        }
    }

    if (!!membershipLoginData || localStorage.getItem("access_token")) {
        history.push(path.mainMenu)
    }

    return (
        <div className="wrapper">
            <div className="sidebar">
            <img  className="img-logo-login" src={logoMI} alt="logoMI"/>
            <p>
                <span>Membership Merintis Indonesia</span> merupakan ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan 
                melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.
            </p>

            </div>
            <div className="main_content">
                <h1 className="h1-login">Hai, Sedulur!</h1>
                <p className="error-login">{ errorLogin === "" ? errorData ?  "email dan password tidak cocok" : "" : errorLogin}</p>
                    <form onSubmit={e => submitLogin(e)}>
                        <div className="login-form">
                            <label className="login-label">EMAIL ADDRESS</label>
                            <input className="input-login" type="text" placeholder="Enter your mail " onChange={inputEmail}/>
                            <label className="login-label">PASSWORD</label>
                            <input className="input-login" type="password" placeholder="Enter your magic spell " onChange={inputPass}/>
                        </div>
                        <div className="form-check">
                            <input type="checkbox" className="checkbox-remember"/>
                            <label className="label-remember">REMEMBER ME?</label>
                            <a href={path.FAQ} className="a-help">BUTUH BANTUAN ?</a>
                        </div>
                        <div className="signin">
                            {isLoading ? <button className="btn-signin"><div className="loader-login"></div></button> : <button className="btn-signin">SIGN IN</button>} 
                        </div>
                    </form>
            </div>
        </div>
    )
}


export default LoginMembership;