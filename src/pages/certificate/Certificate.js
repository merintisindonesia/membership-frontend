import React from "react"
import { useSelector } from "react-redux"

import Navbar from "../../components/navbar/Navbar"

import "./Certificate.css"

function Certificate() {
    const { membershipProfile } = useSelector(state => state.dataMembership)
    
    return (
        <>
            <div className="main-certificate">
            <Navbar/>
                <h2>Sertifikat</h2>
                <div className="info-certificate">
                    <div className="identity">
                        <label className="component-certificate1">Sertifikat Membership  </label>
                        <label className="component-certificate">{membershipProfile.type ? membershipProfile.type : "Perintis"} {membershipProfile.status ? membershipProfile.status : "basic"}   </label>
                        {/* <label className="component-certificate1">Sertifikat Membership  </label>
                        <label className="component-certificate">{membershipProfile.type ? membershipProfile.type : "Perintis"} {membershipProfile.status ? membershipProfile.status : "basic"}   </label> */}
                    </div>

                    <div>
                        <a href="#main-certificate" className="link-certificate1">Lihat Sertifikat  </a>
                        <a href="#main-certificate" className="link-certificate">Download  </a>
                        {/* <a href="#main-certificate" className="link-certificate1">Lihat Sertifikat  </a>
                        <a href="#main-certificate" className="link-certificate">Download  </a> */}
                    </div>
                </div>
            </div>
        </>
    )
}


export default Certificate