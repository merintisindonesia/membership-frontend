import React from "react";
import "./Home.css"
import logoMI from "../../asset/img/Logo-MI.png"
import { useHistory } from "react-router-dom";

import path from "../../routes/route"

function HomePage() {

    const history = useHistory()

    function toLogin() {
        history.push(path.login)
        return
    }


    return (
        <div className="login-page">
            <div id="main-page">
                <div className="header">
                    <img  className="img-logo-home" src={logoMI} alt="logoMI"/>
                    <h1 className="header-name">Selamat Datang di Membership Merintis Indonesia</h1>
                    <h1 className="header-name">  </h1>
                    <button type="button" className="button-kuning" onClick={toLogin}>Login</button>
                </div>
            </div>
        </div>
        
    )
}

export default HomePage;