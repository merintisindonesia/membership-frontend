import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect, Route, useHistory } from 'react-router-dom'
import { registerMembership } from '../../../store/actions/dataMembershipAction'
import { getDataUser } from '../../../store/actions/userAction'

import "./Register.css"

function RegisterMembership(props) {
    const dispatch = useDispatch()
    const history = useHistory()

    const { type, status, userId } = (props.location && props.location.state) || {}

    const { dataUser, isLoading} = useSelector(state => state.user)

    const { membershipRegister, loadingRegister } = useSelector(state => state.dataMembership)

    const [dataObj, setDataObj] = useState({
        placeBirth : "",
        dateBirth : null,
        gender : "",
        agency: "",
        fullAddress : "",
        reasonJoinMembership: "",
    })

    useEffect(() => {
        if(!type || !status) {
            return history.push("/welcome")
        }

        dispatch(getDataUser(userId ? userId : "60"))
    },[])

    console.log(dataObj)
    console.log(membershipRegister)

    const changeData = (e) => {
        e.preventDefault()
        setDataObj({
            ...dataObj,
            [e.target.name] : e.target.value
        })
    }

    const processRegistration = () => {
        let totalAmount = 0
        if(status === "basic") {
            totalAmount = 150000
        } else {
            totalAmount = 350000
        }

        dispatch(registerMembership({
            user_id : Number(userId),
            place_birth : dataObj.placeBirth,
            date_birth : dataObj.dateBirth,
            gender : dataObj.gender,
            agency : dataObj.agency,
            full_address : dataObj.fullAddress,
            reason_join_membership : dataObj.reasonJoinMembership,
            status : status,
            type :type,
            payment_method : "normal",
            amount : totalAmount
        }))
    }

    if(isLoading) {
        return (
            <h1>Loading , mohon tunggu sebentar</h1>
        )
    }

    if (!!dataUser?.type && !!dataUser?.status && !dataUser.is_paid) {
        return (
            <Route path='/payment' component={() => { 
                window.location.href = dataUser.payment_url; 
                return null;
            }}/>
        )
    }

    if(!!dataUser?.type && !!dataUser?.status && dataUser.is_paid) {
        return (
            <Redirect to="/login"/>
        )
    }

    if (!!membershipRegister) {
        return (
            <Route path='/payment' component={() => { 
                window.location.href = dataUser.payment_url; 
                return null;
            }}/>
        )
    }

    return (
        <>
            <h1>Form Pendaftaran</h1>
            <form>
                <div>
                    <input type="text" placeholder="Nama Lengkap" defaultValue={dataUser?.nm_lengkap}/>
                </div>
                <div>
                    <input type="text" name="placeBirth" placeholder="Tempat Lahir" value={dataObj.placeBirth} onChange={e => changeData(e)}/>
                    <input type="date" name="dateBirth" placeholder="Tanggal Lahir" value={dataObj.dateBirth} onChange={e => changeData(e)}/>
                </div>
                <div>
                    <input type="text" name="gender" placeholder="Gender (Laki-laki, perempuan)" value={dataObj.gender} onChange={e => changeData(e)}/>
                </div>
                <div>
                    <input type="text" name="fullAddress" placeholder="Alamat Lengkap" value={dataObj.fullAddress} onChange={e => changeData(e)}/>
                </div>                
                <div>
                    <input type="text" name="agency" placeholder="Asal Sekolah/Universitas/Perusahaan" value={dataObj.agency} onChange={e => changeData(e)}/>
                </div>

                <div>
                    <input type="textarea" name="reasonJoinMembership" placeholder="Alasan kenapa tertarik bergabung membership" value={dataObj.reasonJoinMembership} onChange={e => changeData(e)}/>
                </div>
                <div>
                    <button onClick={processRegistration}>{ loadingRegister ? "Data sedang Diproses" : "Proses Pendaftaran"}</button>
                </div>
            </form>
        </>
    )
}

export default RegisterMembership