import React, { useEffect, useState } from "react"
import Countdown from "react-countdown"
import { useDispatch, useSelector } from "react-redux"
import { Link, useHistory, useParams } from "react-router-dom"
import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import { listAllMemberQuestionExam, setAnswerMemberQuestionExam } from "../../store/actions/memberQuestionExamAction"
import { clearNewMembershipExam, getMemberExamById, membershipExamScore } from "../../store/actions/membershipExamAction"

import ReactModal from "react-modal"

import "./StartExam.css"

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        height: 'fit-content'
    },
};

function ShowModal() {
    const [ modalIsOpen, setModalIsOpen] = useState(true)

    function closeModal() {
        setModalIsOpen(false)
    }

    return <ReactModal
        isOpen={modalIsOpen}
        style={customStyles}>
        <h2 className="modal-h2">Mohon maaf waktu sudah habis ...</h2>
        <button className="btn-modal" onClick={closeModal}>Tampilkan Hasil Score</button>
    </ReactModal>
}

function Renderer({minutes, seconds, completed}) {
    if (completed) {
        return (
            <>
            <span>{"00 : 00"}</span>
            <ShowModal/>
            </>
        )
    } else {
        return <span>{minutes <= 9 ? "0" + minutes : minutes} : {seconds <= 9 ? "0" + seconds : seconds}</span>
    }
}

function StartExam() {
    const history = useHistory()
    const dispatch = useDispatch()
    const { member_exam_id } = useParams()

    const { listQuestionExam, isLoading, loadingSetAnswer} = useSelector(state => state.memberQuestionExam)
    const { membershipExamId } = useSelector(state => state.membershipExam)

    // const [resultExam, setResultExam] = useState(0)

    useEffect(() => {
        dispatch(clearNewMembershipExam())
        dispatch(listAllMemberQuestionExam(member_exam_id))
        dispatch(getMemberExamById(member_exam_id))
    },[member_exam_id])

    const insertAnswer = (e, answer, id) => {
        dispatch(setAnswerMemberQuestionExam(id, answer))
    }

    const submitExamResult = () => {
            let resultExam = 0
            for (let i = 0 ; i < listQuestionExam.length ; i++) {
                if (listQuestionExam[i].answer === listQuestionExam[i].correct_answer) {
                    resultExam++
                }
            }

            dispatch(membershipExamScore(member_exam_id, { score : Math.round((resultExam / listQuestionExam.length) * 100), count_get_exam : 2}))

            history.push({
                pathname: "/score/exam",
                state : {
                    correctResult : resultExam,
                    totalQuestion : listQuestionExam.length,
                    pass : resultExam.length >= 12 ? true : false,
                    finalScore : Math.round((resultExam / listQuestionExam.length) * 100)
                }
            })
    }

    if(listQuestionExam && membershipExamId) {
        console.log(listQuestionExam, membershipExamId)
    }

    if (isLoading || !membershipExamId) {
        return (
        <>
            <Navbar/>
            <h1 className="loading"><div class="loader-main-menu margin-right-3"></div> Loading Get Data </h1>
        </>
        )
    }

    return (
        <>
            <Navbar/>
            <Sidebar/>
            <div className="question-header">
                <h2>Tes Kompetensi</h2>
                <h1 className="time"><Countdown date={Date.now() + ( membershipExamId.time_length ? (new Date(membershipExamId.time_length) - new Date()) : 1800000)} renderer={Renderer}/></h1>
                {/* <h1 className="time"><Countdown date={Date.now()} renderer={Renderer}/></h1> */}
                <Link to="/member/exam">Kembali</Link>
            </div>
            <ul className="question">
            {
                listQuestionExam ? listQuestionExam.map((questionExam, idx) => {
                    return (
                        <>
                            <li key={questionExam.id}>    
                                <h4 key={questionExam.question.slice(0, 10)}>{idx + 1}. {questionExam.question}</h4>
                                <ul className="list-unstyled" key={questionExam.id.slice(14, questionExam.id.length - 1)}>
                                    {
                                        questionExam.choice.map((choice, i) => {
                                            return <li key={questionExam.id.slice(0 + i, 10 + i)}><label><input className="radio-choices" type="radio" value={choice} checked={choice === questionExam.answer} onChange={e => insertAnswer(e, choice, questionExam.id)}/>{choice}</label></li>
                                        })
                                    }
                                </ul>
                            </li>
                        </>
                    )
                }) : <li key="id-question-answer">waiting data</li>
            }
            </ul>
            {loadingSetAnswer ? <button className="btn-gray margin-left-btn-exam">Menyimpan Jawaban</button> : <button className="view-results" onClick={submitExamResult}>Submit</button>} 
        </>
    )
}

export default StartExam

