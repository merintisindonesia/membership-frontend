import React, {useEffect, useState}  from 'react';

import Navbar from '../../components/navbar/Navbar'
import Sidebar from '../../components/sidebar/Sidebar'
import "./ModulePembelajaran.css"

import { useDispatch, useSelector } from "react-redux"
import { useHistory, useParams } from 'react-router-dom';
import { addNotesMembershipContent, doneMembershipContent, getMembershipContentId } from '../../store/actions/membershipContentAction';

function ModulPembelajaran() {
    const dispatch = useDispatch()
    const history = useHistory()

    const { membership_content_id } = useParams()
    const { membershipContent, membershipContentId, loadingMembershiContentId, loadingSaveNotes } = useSelector(state => state.membershipContent)

    const [notes, setNotes] = useState("")
    const [done, setDone] = useState(false)
    const [nextMemberContentId, setNextMemberContentId] = useState("")

    useEffect(() => {
        dispatch(getMembershipContentId(membership_content_id))

        if (membershipContent) {   
            for(let i = 0 ; i < membershipContent.length ; i++) {
                if (membershipContent[i].position === (membershipContentId.position + 1)) {
                    console.log(membershipContent[i])
                    setNextMemberContentId(membershipContent[i].id)
                    break
                }
            }
        }

        if (membershipContentId.notes !== undefined || membershipContentId.done ) {
            setDone(membershipContentId.done)
            setNotes(membershipContentId.notes)
        }
    },[membership_content_id,nextMemberContentId])

    console.log(loadingMembershiContentId, loadingSaveNotes, notes, done)

    function clickDone() {
        dispatch(doneMembershipContent(membershipContentId.id))

        history.push("/show/module/" + nextMemberContentId)
    }

    function addNotes() {
        dispatch(addNotesMembershipContent(membershipContentId.id, notes))
    }

    const notesMembershipContent = (e) => {
        e.preventDefault()
        setNotes(e.target.value)
    }

    if (notes === undefined) {
        return (
            <>
                <Sidebar/>
                <Navbar/>
            </>
        )
    }
    return(
        <>
        <Navbar/>
        <Sidebar/>
        <div className="module">
            {
                !!membershipContentId && !loadingMembershiContentId ? (
                    <>
                        <div className="content-module"> 
                            <h1>{membershipContentId.description}</h1>
                            { membershipContentId.done ? <button className="btn-gray margin-left-btn btncomplete">COMPLETED</button> :<button className="btn-yellow margin-left-btn btncomplete" onClick={clickDone}>COMPLETE</button>} 
                        </div>
                            <iframe src={membershipContentId.video_url} title={membershipContentId.description}></iframe>
                        <div className="content-module2"> 
                        {/* <textarea  className="note" placeholder="NOTE" cols="30" rows="5" onChange={e => notesMembershipContent(e)} value={notes}></textarea> */}
                            {/* <input type="textarea" placeholder="NOTE" onChange={e => setNotes(e.target.value)} value={notes}/> */}
                        {/* { loadingSaveNotes ? <button className="btn-gray margin-top-2">MENYIMPAN..</button> : <button className="btn-yellow margin-top-2" onClick={addNotes}>SIMPAN NOTES</button>}  */}
                        </div>
                    </>
                )  : <p></p>
            }
        </div>
        </>
    )
}


export default ModulPembelajaran