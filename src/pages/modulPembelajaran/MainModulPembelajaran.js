import Sidebar from "../../components/sidebar/Sidebar"
import Navbar from "../../components/navbar/Navbar"

import "./MainModulPembelajaran.css"
import { useDispatch, useSelector } from "react-redux"
import { useEffect, useState } from "react"
import { useHistory } from "react-router-dom"
import { getMembershipProfile } from "../../store/actions/dataMembershipAction"
import { getMembershipContent } from "../../store/actions/membershipContentAction"
function MainModulPembelajaran() {
    const dispatch = useDispatch()
    const history = useHistory()

    const { membershipContent, isLoading } = useSelector(state => state.membershipContent)
    const { membershipProfile } = useSelector(state => state.dataMembership)
    const [ startShowContent, setStartShowContent ] = useState("")

    useEffect(() => {
        if(!membershipProfile) {
            dispatch(getMembershipProfile())
        }

        if(!membershipContent) {
            dispatch(getMembershipContent())
        }

        if(membershipContent) {
            for (let i = 0 ; i < membershipContent.length ; i++) {
                if (membershipContent[i].done === false) {
                    setStartShowContent(membershipContent[i].id)
                    break
                }
            }
        }
    },[])

    const startModule = () => {
        history.push("/show/module/" + startShowContent)
    }

    if (isLoading || !membershipProfile) {
        return (
            <>
                <Navbar/>
                <Sidebar/>
            </>
        )
    }

    return (
        <>
            <Navbar/>
            <Sidebar/>
            <div className="main-modul">
                <h1>Modul Pembelajaran</h1>
                <h2>{membershipProfile.type} {membershipProfile.status}</h2>
                <p>Modul pembelajaran Pengembang Premium dikembangkan untuk kamu yang ingin mengembangkan dan mengoptimalkan bisnis yang telah ada. Kurikulum terdiri atas 20 topik dan terdapat Tes Kompetensi di akhir pembelajaran.</p>
                <h2>Topik</h2>
                <p className="desc-topic">{ membershipContent ? membershipContent.length : "0"} Topik | 1 Fasilitator | 2 Jam Video </p>
                <ul className="list-topic">
                    {
                        membershipContent && membershipContent.map((content) => {
                            return <li key={content.id} className="topic">{content.title} : {content.description}</li> 
                        })
                    }
                </ul>
                {/* {!!startShowContent ? <button onClick={startModule}>Mulai Belajar</button> : <button>Please wait</button>}  */}
            </div>
        </>
    )
}

export default MainModulPembelajaran