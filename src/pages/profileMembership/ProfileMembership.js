import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"

import Navbar from "../../components/navbar/Navbar"

import { getMembershipProfile } from "../../store/actions/dataMembershipAction"

import "./ProfileMembership.css"

function ProfileMembership() {
    const dispatch = useDispatch()

    const { isLoading, membershipProfile} = useSelector(state => state.dataMembership)

    useEffect(() => {
        if(!membershipProfile) {
            dispatch(getMembershipProfile())
        }
    },[])

    if (isLoading) return (
        <h1 className="loading"><div class="loader-main-menu margin-right-3"></div>   Loading get data profile membership</h1>
    )

    return (
        <div className="main-profile">
            <Navbar/>
            <h2>Profil Pengguna</h2>
            <div className="info-profile">
                {/* <div>
                    <label className="component-profile">Nama Lengkap : </label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.nm_lengkap : ""}</label>
                </div>
                <div>
                    <label className="component-profile">Email : </label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.email : ""}</label>
                </div>
                <div>
                     <label className="component-profile">Email : </label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.no_hp: ""}</label>
                </div>
                <div>
                    <label className="component-profile">Nama Sertifikat : </label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.certificate_name : ""}</label>
                </div> */}
                <div className="identity">
                    <label className="component-profile">Nama Lengkap  </label>
                    <label className="component-profile">Email  </label>
                    <label className="component-profile">No Telepon  </label>
                    <label className="component-profile">Nama Sertifikat  </label>
                </div>

                <div>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.nm_lengkap : ""}</label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.email : ""}</label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.no_hp: ""}</label>
                    <label className="value-profile">{!!membershipProfile ? membershipProfile.certificate_name : ""}</label>
                </div>

            </div>
            {/* <button>edit data</button> */}
        </div>
    )
}

export default ProfileMembership