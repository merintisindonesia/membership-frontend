import React from 'react'

import { Route, Redirect } from 'react-router'

function OnlyPublicRoute(props) {
    const authUser = !!localStorage.getItem("access_token")

    return !authUser ? <Route {...props}>{props.children}</Route> : <Redirect to="/main-menu" />
}

export default OnlyPublicRoute