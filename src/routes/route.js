const path = {
    login : "/login",
    mainMenu : "/main-menu",
    module : "/show/module/:membership_content_id",
    mainModule : "/main/module",
    talkshow : "/talkshow",
    memberProfile : "/member/profile",
    certificate : "/member/certificate",
    memberExam : "/member/exam",

    startExam : "/start/exam/:member_exam_id",
    resultScore : "/score/exam",

    memberInfoStart : "/member/info",
    memberRegister: "/member/registration",
    memberPayment : "/member/payment",

    landingPage : "/welcome",

    FAQ : "/faq",
    root : "/",
    any : "*"
}


export default path