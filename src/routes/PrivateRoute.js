import React from 'react'
import { Route, Redirect } from 'react-router'

function PrivateRoute(props) {
    const authUser = !!localStorage.getItem("access_token")

    return authUser ? <Route {...props}>{props.children}</Route> : <Redirect to="/login" />
}

export default PrivateRoute