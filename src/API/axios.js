import axios from 'axios';

const initAxios = axios.create({
    baseURL: "https://membership-merintisindonesia.herokuapp.com"
    // baseURL: "http://localhost:8080"
})

export default initAxios;