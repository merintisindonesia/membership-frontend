const initialState = {
    exams : null,
    isLoading: false,
    errors : null
}

const examReducer = (state = initialState, action) => {
    switch (action.type) {
        case "EXAM_LOADING":
            return {
                ...state,
                isLoading : true
            }
        case "EXAM_ERROR":
            return {
                ...state,
                errors : action.payload
            }
        case "DATA_EXAM_SHUFFLE":
            return {
                ...state,
                exams : action.payload,
                isLoading: false
            }
        default:
            return state
    }
}

export default examReducer