const initialState = {
    membershipLoginData : "",
    membershipProfile: null,
    membershipRegister : null,
    loadingRegister: false,
    isLoading: false,
    errorData : null
}

const dataMembershipReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING":
            return {
                ...state,
                isLoading : true
            }
        case "REGISTER_MEMBERSHIP_LOADING":
            return {
                ...state,
                loadingRegister: true
            }
        case "SET_ERROR":
            return {
                ...state,
                errorData : action.payload,
                isLoading: false
            }
        case "CLEAR_SET_ERROR":
            return {
                ...state,
                errorData: null
            }
        case "LOGIN":
            return {
                ...state,
                membershipLoginData : action.payload,
                isLoading: false
            }
        case "SUCCESS_REGISTER_MEMBERSHIP":
            return {
                ...state,
                membershipRegister: action.payload,
                loadingRegister: false
            }
        case "PROFILE_MEMBERSHIP":
            return {
                ...state,
                membershipProfile: action.payload,
                isLoading: false
            }
        case "LOGOUT":
            localStorage.removeItem("access_token")
            localStorage.removeItem("role")
            return {
                ...state,
                membershipLoginData: ""
            }
        default:
            return state
    }
}

export default dataMembershipReducer