const initialState = {
    contentId : null,
    isLoading : false,
    errors : null
}

const contentReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_CONTENT_LOADING":
            return {
                ...state,
                isLoading : true
            }
        case "SET_CONTENT_ERROR":
            return {
                ...state,
                errors : action.payload
            }
        default:
            return state
    }
}

export default contentReducer