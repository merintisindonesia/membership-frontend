import sortMemberContent from "../../helper/sorting"
// import changeDataContent from "../../helper/changeData"

const initialState = {
    membershipContent : "",
    membershipContentId : {},
    isLoading: false,
    loadingMembershiContentId : false,
    loadingSaveNotes : false,
    errors : null
}

const membershipContentReducer = (state = initialState, action) => {
    switch (action.type) {
        case "MEMBER_CONTENT_LOADING":
            return {
                ...state,
                isLoading : true
            }
        case "MEMBER_ID_CONTENT_SET_LOADING":
            return {
                ...state,
                loadingMembershiContentId : true
            }
        case "MEMBER_CONTENT_NOTES_LOADING":
            return {
                ...state,
                loadingSaveNotes: true
            }
        case "MEMBER_CONTENT_ERROR":
            return {
                ...state,
                errors : action.payload,
                isLoading: false,
                loadingSaveNotes: false,
                loadingMembershiContentId: false
            }
        case "CLEAR_MEMBER_CONTENT_ID":
            return {
                ...state,
                membershipContentId : {}
            }
        case "DATA_MEMBER_CONTENT":
            return {
                ...state,
                membershipContent : sortMemberContent(action.payload),
                isLoading: false
            }
        case "GET_DATA_MEMBER_CONTENT_ID":
            return {
                ...state,
                membershipContentId : action.payload,
                loadingMembershiContentId : false
            }
        case "DONE_MEMBERHSIP_CONTENT":
            return {
                ...state,
                loadingMembershiContentId: false
            }
        case "ADD_NOTES_MEMBERSHIP_CONTENT":
            return {
                ...state,
                loadingSaveNotes: false
            }
        default:
            return state
    }
}

export default membershipContentReducer