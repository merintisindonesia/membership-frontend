import sortingLatestDate from "../../helper/sortingLatestDate"

const initialState = {
    membershipExams : null,
    newMembershipExam : null,
    membershipExamId : null,
    listQuestionByMembershipExam : null,
    isLoading : false,
    loadingNewMemberExam: false,
    errors : null
}

const membershipExamReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_MEMBER_EXAM_LOADING":
            return {
                ...state,
                isLoading : true
            }
        case "SET_NEW_MEMBER_EXAM_LOADING":
            return{
                ...state,
                loadingNewMemberExam: true
            }
        case "SET_MEMBER_EXAM_ERROR":
            return {
                ...state,
                errors : action.payload,
                isLoading: false,
                loadingNewMemberExam: false
            }
        case "CLEAR_NEW_MEMBERSHIP_EXAM":
            return {
                ...state,
                newMembershipExam : null
            }
        case "DATA_MEMBER_EXAM":
            return {
                ...state,
                membershipExams : sortingLatestDate(action.payload),
                newMembershipExam : null,
                isLoading: false
            }
        case "GET_MEMBER_EXAM_ID" :
            return {
                ...state,
                membershipExamId: action.payload,
                isLoading: false,
            }
        case "SHOW_LIST_EXAM_START":
            return {
                ...state,
                listQuestionByMembershipExam : action.payload,
                isLoading: false,
            }
        case "UPDATE_SCORE_MEMBER_EXAM":
            return {
                ...state,  
                membershipExams: null,
                isLoading: false,
            }
        case "CREATE_NEW_MEMBERSHIP_EXAM":
            return {
                ...state,
                newMembershipExam : action.payload,
                loadingNewMemberExam: false,
                isLoading: false,
            }
        default:
            return state
    }
}

export default membershipExamReducer