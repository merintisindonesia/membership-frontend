const initialState = {
    talkshows : null,
    isLoading: false,
    errors : null
}

const talkshowReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_TALKSHOW_LOADING":
            return {
                ...state,
                isLoading : true
            }
        case "SET_TALKSHOW_ERROR":
            return {
                ...state,
                errors : action.payload,
                isLoading: false
            }
        case "SET_DATA_TALKSHOW":
            return {
                ...state,
                talkshows : action.payload,
                isLoading: false
            }
        default:
            return state
    }
}

export default talkshowReducer