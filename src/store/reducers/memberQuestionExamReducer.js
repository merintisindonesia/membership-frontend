const initialState = {
    isLoading : false,
    loadingSetAnswer : false,
    error : null,
    listQuestionExam  :null,
}


const memberQuestionExamReducer = (state = initialState, action) => {
    switch (action.type) {
        case "QUESTION_EXAM_MEMBERHSIP_LOADING":
            return {
                ...state,
                isLoading: true
            }
        case "LOADING_SET_ANSWER_QUESTION_EXAM":
            return {
                ...state,
                loadingSetAnswer: true
            }
        case "QUESTION_EXAM_MEMBERSHIP_ERROR":
            return {
                ...state,
                error : action.payload,
                isLoading: false
            }
        case "LIST_QUESTION_EXAM_MEMBERSHIP":
            return {
                ...state,
                listQuestionExam: action.payload,
                isLoading: false
            }
        case "ANSWER_QUESTION_EXAM_MEMBERSHIP":
            return {
                ...state,
                loadingSetAnswer: false
            }
        default:
            return state
    }
}

export default memberQuestionExamReducer