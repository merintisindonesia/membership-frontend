const initialState = {
    dataUser : null,
    isLoading: false,
    errors : null
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case "LOADING_GET_USER":
            return {
                ...state,
                isLoading : true
            }
        case "SET_ERROR_USER":
            return {
                ...state,
                errors : action.payload,
                isLoading: false
            }
        case "DATA_MEMBERSHIP_USER":
            return {
                ...state,
                dataUser : action.payload,
                isLoading: false
            }
        default:
            return state
    }
}

export default userReducer