import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import contentReducer from "./reducers/contentReducer";
import dataMembershipReducer from "./reducers/datamembershipReducer";
import examReducer from "./reducers/examReducer";
import memberQuestionExamReducer from "./reducers/memberQuestionExamReducer";
import membershipContentReducer from "./reducers/membershipContentReducer";
import membershipExamReducer from "./reducers/membershipExamReducer";
import talkshowReducer from "./reducers/talkshowReducer";
import userReducer from "./reducers/userReducer";

const rootReducer = combineReducers({
    dataMembership : dataMembershipReducer,
    membershipContent : membershipContentReducer,
    membershipExam : membershipExamReducer,
    memberQuestionExam : memberQuestionExamReducer,
    contentData : contentReducer,
    talkshow : talkshowReducer,
    exam : examReducer,
    user: userReducer
})

const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
)

export default store