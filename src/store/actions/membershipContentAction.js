import axios from "../../API/axios" 

export const loadingMemberContent = () => {
    return {
        type : "MEMBER_CONTENT_LOADING"
    }
}

export const loadingMemberContentId = () => {
    return {
        type : "MEMBER_ID_CONTENT_SET_LOADING"
    }
}

export const loadingSaveNotes = () => {
    return {
        type : "MEMBER_CONTENT_NOTES_LOADING"
    }
}

export const errorMemberContent = (payload) => {
    return {
        type : "MEMBER_CONTENT_ERROR",
        payload
    }
}

export const clearMemberContentId = () => {
    return {
        type : "CLEAR_MEMBER_CONTENT_ID"
    }
}

export const getMembershipContent = () => {
    return async dispatch => {
        try {
            dispatch(loadingMemberContent())

            const { data } = await axios({
                method: "GET",
                url: "/v1/membership_contents",
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch({
                type : "DATA_MEMBER_CONTENT",
                payload : data
            })

        } catch (err) {
            return err?.response?.data ?
            dispatch(errorMemberContent( err.response.data)) : 
            console.log(err)
        }
    }
}

export const getMembershipContentId = (id) => {
    return async dispatch => {
        try {
            dispatch(loadingMemberContentId())

            dispatch(clearMemberContentId())
            const { data } = await axios({
                method : "GET",
                url :  "v1/membership_contents/" + id,
                headers: {
                    Authorization : localStorage.getItem("access_token")
                } 
            })

            return dispatch({
                type : "GET_DATA_MEMBER_CONTENT_ID",
                payload : data
            })
        } catch (err) {
            return err?.response?.data ?
            dispatch(errorMemberContent( err.response.data)) : 
            console.log(err)
        }
    }
}

export const doneMembershipContent = (membershipContentId) => {
    return async dispatch => {
        try {
            
            dispatch(loadingMemberContentId())

            const { data } = await axios( {
                method: "PUT",
                url: `/v1/membership_contents/${membershipContentId}/done`,
                headers: {
                    Authorization : localStorage.getItem("access_token")
                } 
            })

            return dispatch({
                type : "DONE_MEMBERHSIP_CONTENT",
                payload : data
            })

        } catch (err) {
            return err?.response?.data ?
            dispatch(errorMemberContent( err.response.data)) : 
            console.log(err)
        }
    }
}

export const addNotesMembershipContent = (membershipContentId, notes) => {
    return async dispatch => {
        try {

            dispatch(loadingSaveNotes())

            const { data } = await axios({
                method: "PUT",
                url: `/v1/membership_contents/${membershipContentId}/notes`,
                data: {
                    notes : notes
                },
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch ({
                type: "ADD_NOTES_MEMBERSHIP_CONTENT",
                payload : data
            })
            
        } catch (err) {
            return err?.response?.data ?
            dispatch(errorMemberContent( err.response.data)) : 
            console.log(err)
        }
    }
}