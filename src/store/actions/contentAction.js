import axios from "../../API/axios"

export const contentLoading = () => {
    return ( {
        type : "SET_CONTENT_LOADING"
        }
    )
}

export const contentError = (payload) => {
    return (
        {
            type : "SET_CONTENT_ERROR",
            payload
        }
    )
}

// export const clearDataContentId = () => {
//     return {
//         type : "CLEAR_DATA_CONTENT_ID"
//     }
// }

// export const getContentById = (id) => {
//     return async dispatch => {
//         try {
//             dispatch(contentLoading())

//             dispatch(clearDataContentId())

//             const { data } = await axios({
//                 method: "GET",
//                 url : "/v1/contents/" + id,
//                 headers: {
//                     Authorization : localStorage.getItem("access_token")
//                 }
//             }) 

//             return dispatch({
//                 type : "DATA_CONTENT_ID",
//                 payload: data,
//             })
//         } catch (err) {
//             return err?.response?.data?.messages ?
//             dispatch(contentError(err.response.data.messages)) : 
//             console.log(err)
//         }
//     }
// }