import axios from "../../API/axios"

export const loadingExam = () => {
    return {
        type : "EXAM_LOADING"
    }
}

export const errorExam = (payload) => {
    return {
        type : "EXAM_ERROR",
        payload
    }
}

export const getExam = () => {
    return async dispatch => {
        try {
            dispatch(loadingExam())

            const { data } = await axios({
                method: "GET",
                url : "/v1/exams/start",
                headers: {
                    Authorization: localStorage.getItem("access_token")
                }
            })

            dispatch({
                type : "DATA_EXAM_SHUFFLE",
                payload : data
            })

        } catch (err) {
            return err?.response?.data?.messages ?
            dispatch(errorExam( err.response.data.messages)) : 
            console.log(err)  
        }
    }
}