import axios from "../../API/axios" 

export const memberExamLoading = () => {
    return {
        type : "SET_MEMBER_EXAM_LOADING"
    }
}

export const newMemberExamLoading = () => {
    return {
        type: "SET_NEW_MEMBER_EXAM_LOADING"
    }
}

export const memberExamError = (payload) => {
    return {
        type : 'SET_MEMBER_EXAM_ERROR',
        payload
    }
}

export const clearNewMembershipExam = () => {
    return {
        type : "CLEAR_NEW_MEMBERSHIP_EXAM"
    }
}

export const getMembershipExam = () => {
    return async dispatch => {
        try {

            dispatch(memberExamLoading())

            const { data } = await axios({
                method: "GET",
                url : "/v1/membership_exams",
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch({
                type : "DATA_MEMBER_EXAM",
                payload: data
            })
        } catch (err) {
            return err?.response?.data?.messages ?
            dispatch(memberExamError( err.response.data.messages)) : 
            console.log(err)
        }
    }
}

export const getMemberExamById = (memberExamId) => {
    return async dispatch => {
        try {
            dispatch(memberExamLoading())

            const { data } = await axios({
                method: "GET",
                url: "/v1/membership_exams/" + memberExamId,
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch({
                type : "GET_MEMBER_EXAM_ID",
                payload: data
            })
        } catch (err) {
            return err?.response?.data ?
            dispatch(memberExamError( err.response.data)) : 
            console.log(err)
        }
    }
}

export const createNewMembershipExam = () => {
    return async dispatch => {
        try {
            dispatch(newMemberExamLoading())

            const { data } = await axios(
                {
                    method: "POST",
                    url: "/v1/membership_exams",
                    headers: {
                        Authorization: localStorage.getItem("access_token")
                    }
                }
            )
            
            return dispatch({
                type : "CREATE_NEW_MEMBERSHIP_EXAM",
                payload : data
            })
        } catch (err) {
            return err?.response?.data?.messages ?
            dispatch(memberExamError( err.response.data.messages)) : 
            console.log(err)
        }
    }
}

export const showMembershipExamStart = (memberExamId) => {
    return async dispatch => {
        try {

            const { data } = await axios({
                method: "GET",
                url: "/v1/membership_question_exams/membership_exam/" + memberExamId
            })

            return dispatch({
                type : "SHOW_LIST_EXAM_START",
                payload : data
            })
        } catch (err) {
            return err?.response?.data?.messages ?
            dispatch(memberExamError( err.response.data.messages)) : 
            console.log(err)
        }
    }
}

export const membershipExamScore = (memberExamId, score) => {
    return async dispatch => {
        try {
            dispatch(memberExamLoading())
            
            const { data } = await axios({
                method:"POST",
                url: "/v1/membership_exams/" + memberExamId,
                data: score,
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            console.log(data)

            return dispatch({
                type : "UPDATE_SCORE_MEMBER_EXAM",
                payload : data
            })
        } catch (err) {
            console.log(err)
            return err?.response?.data ?
            dispatch(memberExamError( err.response.data)) : 
            console.log(err)
        }
    }
}