import axios from "../../API/axios"

export function setLoading() {
    return{
        type : "SET_LOADING"
    }
}

export function registerMembershipLoading() {
    return {
        type : "REGISTER_MEMBERSHIP_LOADING"
    }
}

export const setError = (payload) => {
    return {
        type : "SET_ERROR",
        payload
    }
}

export const clearError = () => {
    return {
        type: "CLEAR_SET_ERROR"
    }
}

export const membershipLogout = () => {
    return {
        type : "LOGOUT"
    }
}

export const getMembershipProfile = () => {
    return async dispatch => {
        try {
            dispatch(setLoading())

            const {data} = await axios({
                method: "GET",
                url: "/v1/memberships/profile",
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            dispatch({
                type : "PROFILE_MEMBERSHIP",
                payload: data
            })

            return
        } catch (err) {
            return err?.response?.data ?
            dispatch({ type: "SET_ERROR", payload: err.response.data}) : 
            console.log(err)
        }
    }
}

export const membershipLogin = payload => {
    return async dispatch => {
        try {
            dispatch(clearError())
            dispatch(setLoading())
            
            const { data } = await axios({
                method : "POST",
                url : "/v1/memberships/login",
                data: payload,
            })

            localStorage.setItem("access_token", data.token)
            localStorage.setItem("role", data.role)

            return dispatch({ type : "LOGIN", payload: data})
        } catch (err) {
            return err?.response?.data ?
            dispatch({ type: "SET_ERROR", payload: err.response.data}) : 
            console.log(err)
        }
    }
}

export const registerMembership = (dataObj) => {
    return async dispatch => {
        try {

            dispatch(registerMembershipLoading())

            const {data} = await axios({
                method: "POST",
                url: "/v1//memberships/create",
                data: dataObj
            })

            return dispatch({
                type : "SUCCESS_REGISTER_MEMBERSHIP",
                payload : data
            })
        } catch (err) {
            err?.response?.data ? dispatch({ type : "SET_ERROR", payload : err.response.data}) : console.log(err)
        }
    }
}