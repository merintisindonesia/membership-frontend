import axios from "../../API/axios" 

export const loadingMemberQuestionExam = () => {
    return {
        type : "QUESTION_EXAM_MEMBERHSIP_LOADING"
    }
}

export const loadingSetAnswerMemberQuestionExam = () => {
    return {
        type : "LOADING_SET_ANSWER_QUESTION_EXAM"
    }
}

export const errorMemberQuestionExam = (payload) => {
    return {
        type : "QUESTION_EXAM_MEMBERSHIP_ERROR",
        payload
    }
} 

export const listAllMemberQuestionExam = (membershipExamId) => {
    return async dispatch => {
        try {
            dispatch(loadingMemberQuestionExam())
            
            const { data } = await axios({
                method: "GET",
                url: "/v1/membership_question_exams/membership_exam/" + membershipExamId,
                headers: {  
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch({
                type : "LIST_QUESTION_EXAM_MEMBERSHIP",
                payload : data
            })

        } catch (err) {
            return err?.response?.data ?
            dispatch(errorMemberQuestionExam( err.response.data)) : 
            console.log(err)
        }
    }
}

export const setAnswerMemberQuestionExam = (memberQuestExamId, answer) => {
    return async dispatch => {
        try {
            dispatch(loadingSetAnswerMemberQuestionExam())

            const { data } = await axios({
                method : "PATCH",
                url: `/v1/membership_question_exams/${memberQuestExamId}/answer`,
                data : {
                    answer : answer
                },
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch({
                type : "ANSWER_QUESTION_EXAM_MEMBERSHIP",
                payload : data
            })
        } catch (err) {
            return err?.response?.data ?
            dispatch(errorMemberQuestionExam( err.response.data)) : 
            console.log(err)
        }
    }
}