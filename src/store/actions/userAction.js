import axios from "../../API/axios" 

export const loadingUser = () => {
    return {
        type : "LOADING_GET_USER"
    }
}

export const errorUser = (payload) => {
    return {
        type : "SET_ERROR_USER",
        payload
    }
}

export const getDataUser = (id) => {
    return async dispatch => {
        try {

            dispatch(loadingUser())

            const { data } = await axios({
                method: "GET",
                url : `/v1/users/${id}/membership`
            })

            return dispatch({
                type : "DATA_MEMBERSHIP_USER",
                payload: data
            })

        } catch (err) {
            err?.response?.data ? dispatch(errorUser(err.response.data)) : console.log(err.response)
        }
    }
}