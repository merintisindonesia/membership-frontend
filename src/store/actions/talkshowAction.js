import axios from "../../API/axios"

export function setTalkshowLoading() {
    return{
        type : "SET_TALKSHOW_LOADING"
    }
}

export function setTalkshowError(payload) {
    return {
        type : "SET_TALKSHOW_ERROR",
        payload
    }
}

export const dataTalkshowVideos = () => {
    return async dispatch => {
        try {
            dispatch(setTalkshowLoading())

            const { data } = await axios({
                method : "GET",
                url : "/v1/talkshow_videos",
                headers: {
                    Authorization : localStorage.getItem("access_token")
                }
            })

            return dispatch({ type : "SET_DATA_TALKSHOW", payload: data})
        } catch (err) {
            return err?.response?.data ?
            dispatch({ type: "SET_TALKSHOW_ERROR", payload: err.response.data}) : 
            console.log(err)
        }
    }
}