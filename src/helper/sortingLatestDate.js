
function sortingLatestDate(arrObj) {
    return arrObj.sort((a, b) => {
        return new Date(b.created_at) - new Date(a.created_at)
    }).slice(0,6)
}

export default sortingLatestDate