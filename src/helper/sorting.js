
function sortMemberContent(arrObj) {
    let swap = true

    while (swap) {
        swap = false

        for (let i = 0 ; i < arrObj.length -1 ; i++ ) {
            if (arrObj[i].position > arrObj[i+1].position) {
                let temp = arrObj[i]
                arrObj[i] = arrObj[i+1]
                arrObj[i+1] = temp

                swap = true
            }
        }
    }

    return arrObj
}

export default sortMemberContent